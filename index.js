const express = require('express');
const app = express()
const port = 3100;
const host = "localhost";

app.get('/', (req, res) => res.send('Welcome!'));

app.listen(port,host, () => console.log(`Example app listening on port ${port}!`))
